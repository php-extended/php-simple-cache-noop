# php-extended/php-simple-cache-noop
A psr-16 compliant cache that caches nothing, for mocking purposes.

![coverage](https://gitlab.com/php-extended/php-simple-cache-noop/badges/master/pipeline.svg?style=flat-square)
![build status](https://gitlab.com/php-extended/php-simple-cache-noop/badges/master/coverage.svg?style=flat-square)


## Installation

The installation of this library is made via composer and the autoloading of
all classes of this library is made through their autoloader.

- Download `composer.phar` from [their website](https://getcomposer.org/download/).
- Then run the following command to install this library as dependency :
- `php composer.phar php-extended/php-simple-cache-noop ^8`


## Basic Usage

This library is to make a man in the middle for http requests and responses
and logs the events when requests passes. It may be used the following way :

```php

use PhpExtended\SimpleCache\SimpleCacheNoop;

$cache = new SimpleCacheNoop();

$itemToStore = '<data>';

$cache->set($key, $itemToStore);

$data = $cache->get($key);	// retrieves nothing

```


## License

MIT (See [license file](LICENSE)).
