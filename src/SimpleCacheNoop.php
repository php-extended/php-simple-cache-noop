<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-simple-cache-noop library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\SimpleCache;

use DateInterval;
use Psr\SimpleCache\CacheInterface;
use Stringable;

/**
 * SimpleCacheNoop class file.
 * 
 * This class is a cache that caches nothing.
 * 
 * @author Anastaszor
 */
class SimpleCacheNoop implements CacheInterface, Stringable
{
	
	/**
	 * {@inheritDoc}
	 * @see \Stringable::__toString()
	 */
	public function __toString() : string
	{
		return static::class.'@'.\spl_object_hash($this);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \Psr\SimpleCache\CacheInterface::get()
	 * @param string $key
	 * @param null|boolean|integer|float|string|object|array<integer|string, null|boolean|integer|float|string|object|array<integer|string, null|boolean|integer|float|string|object>> $default
	 * @return null|boolean|integer|float|string|object|array<integer|string, null|boolean|integer|float|string|object|array<integer|string, null|boolean|integer|float|string|object>>
	 * @psalm-suppress MoreSpecificImplementedParamType
	 */
	public function get(string $key, mixed $default = null) : mixed
	{
		return $default;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \Psr\SimpleCache\CacheInterface::set()
	 */
	public function set(string $key, mixed $value, null|int|DateInterval $ttl = null) : bool
	{
		return true;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \Psr\SimpleCache\CacheInterface::delete()
	 */
	public function delete(string $key) : bool
	{
		return true;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \Psr\SimpleCache\CacheInterface::clear()
	 */
	public function clear() : bool
	{
		return true;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \Psr\SimpleCache\CacheInterface::getMultiple()
	 * @param iterable<string> $keys
	 * @param null|boolean|integer|float|string|object|array<integer|string, null|boolean|integer|float|string|object|array<integer|string, null|boolean|integer|float|string|object>> $default
	 * @return iterable<null|boolean|integer|float|string|object|array<integer|string, null|boolean|integer|float|string|object|array<integer|string, null|boolean|integer|float|string|object>>>
	 * @psalm-suppress MoreSpecificImplementedParamType,LessSpecificImplementedReturnType
	 */
	public function getMultiple(iterable $keys, mixed $default = null) : iterable
	{
		$data = [];
		
		foreach($keys as $key)
		{
			$data[$key] = $default;
		}
		
		return $data;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \Psr\SimpleCache\CacheInterface::setMultiple()
	 * @param iterable<string> $values
	 * @param null|int|DateInterval $ttl
	 * @psalm-suppress MoreSpecificImplementedParamType
	 */
	public function setMultiple(iterable $values, null|int|DateInterval $ttl = null) : bool
	{
		return true;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \Psr\SimpleCache\CacheInterface::deleteMultiple()
	 * @param iterable<string> $keys
	 */
	public function deleteMultiple(iterable $keys) : bool
	{
		return true;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \Psr\SimpleCache\CacheInterface::has()
	 */
	public function has(string $key) : bool
	{
		return false;
	}
	
}
